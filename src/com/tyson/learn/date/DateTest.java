package com.tyson.learn.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTest {
    public static void main(String[] args) {
        //获取现在系统时间
        Date date = new Date();
        System.out.println(date);

        //日期格式转换 MM代表月 mm代表分钟
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd/HH:mm:ss EEEEE");
        //调用format方法参数是date
        //返回结果类型是String
        String format = simpleDateFormat.format(date);
        System.out.println(format);


        //Canlendar日期类 设置时间
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR,2021);
        c.set(Calendar.MONDAY,0);
        c.set(Calendar.DAY_OF_MONTH,3);



        //设置目前时间

        c.setTime(new Date());

        //计算两天后的日期

        int days = c.get(Calendar.DAY_OF_MONTH);
        days += 2;
        c.set(Calendar.DAY_OF_MONTH,days);
        System.out.println(simpleDateFormat.format(c.getTime()));
        //或者使用add方法 表示加几天，-表示减去几天
        //两天后
        c.add(Calendar.DAY_OF_MONTH,2);
        System.out.println(simpleDateFormat.format(c.getTime()));
        //往前两天 用-
        c.add(Calendar.DAY_OF_MONTH,-2);
        System.out.println(simpleDateFormat.format(c.getTime()));

        //计算今天所在的周是2020年的第⼏周
        int week = c.get(Calendar.WEEK_OF_YEAR);
        System.out.println(week);
        Date time = c.getTime();
        System.out.println(time);



    }
}
